This repository contains a fork from http://www.librevisa.org with the intent to add features. For example, attributes are not implemented in the original. Particularly the timeout attribute I consider important. The Git repository of the original is read-only.

The original project implements the VISA API (see http://www.ivifoundation.org/specifications/default.aspx) as an Open Source shared Linux library (libvisa.so) in contrast to Windows libraries which are available as Closed Source from Tektronix or National Instrument and require registration for download.

For the make process, the original uses automake tools, but I have added a layer on top of it using the C++ NetBeans plugin.

Contact: gfuchs@acousticmicroscopy.com