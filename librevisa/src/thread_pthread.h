/* 
 * Copyright (C) 2013 Simon Richter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef librevisa_thread_pthread_h_
#define librevisa_thread_pthread_h_ 1

#include "mutex.h"
#include "condvar.h"
#include "lock.h"
#include "exception.h"
#include "logger.h"

#include <pthread.h>
#include <signal.h>
#include <stdio.h>


extern "C" void *thread_func(void *arg);

namespace librevisa {

/// @todo Move function implementations (start, kill) to the .cpp module.
///       Maybe that was done to "separate" C and C++ code.
class thread
{
public:
        class runnable
        {
        public:
                virtual void init(int timeout) { }
//                virtual void init() { }
                virtual void run() = 0;
        };

        thread(runnable &r) : sui(r) { }

        void start()
        {
                lock locked(sui.cs);
                if(sui.running)
                        return;

                pthread_create(&handle, 0, &thread_func, &sui);
                LOG("Started thread with handle 0x%016lX \n", handle);

                sui.cv.wait(sui.cs);
        }

        void kill(int sig)
        {
          try {
            // The mutex might not be waiting yet. In that case, sending the 
            // user signal will cause an application to exit since the signal is
            // not being caught.
            if (sui.cs.impl.__data.__count > 0) {
              // This thread library function does not really kill a thread.
              // It just sends a signal.
              pthread_kill(handle, sig);
            }
//            LOG("Sent signal %d to thread with handle 0x%016lX.\n", sig, handle);
          }
          catch(exception e) {
            LOG("Exception 0x%08X\n", (unsigned) e.code);
          }
        }

private:
        struct startupinfo
        {
                startupinfo(runnable &r) : r(r), running(false) { }

                runnable &r;
                mutex cs;
                condvar cv;
                bool running;
                unsigned timeout;
        };

        startupinfo sui;

        pthread_t handle;

        friend void *::thread_func(void *);
};

}

#endif
