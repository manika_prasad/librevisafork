/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   logger.h
 * Author: gfuchs
 *
 * Created on October 18, 2018, 3:10 PM
 */

#ifndef LOGGER_H
#define LOGGER_H

#ifdef __cplusplus
extern "C" {
#endif

#ifdef DEBUG
  #include <stdio.h>
  #define LOG(format, ...) (void)printf(format, ##__VA_ARGS__) 
#else
  #define LOG(format, ...) 
#endif
  


#ifdef __cplusplus
}
#endif

#endif /* LOGGER_H */

