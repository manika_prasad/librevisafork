/* 
 * Copyright (C) 2013 Simon Richter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
  #include <config.h>
#endif

#include "messagepump.h"

#include "timeval_op.h"
#include "lock.h"
#include "logger.h"

#include <queue>

#include <sys/time.h>
#include <sys/signal.h>
#include <stdio.h>
#include <iostream>


using std::cout;

namespace librevisa {

messagepump::messagepump() throw() :
        worker(*this)
{
        return;
}

void messagepump::register_watch(watch &w)
{
        worker.start();
        lock l(cs);
        watches.push_front(w);
        LOG("Registered watch. fd = %d. event = %d.\n", w.fd, w.event);
        poke_worker();
}

void messagepump::unregister_watch(watch &w)
{
        lock l(cs);
        LOG("Unregistered watch, fd = %d\n", w.fd);
        w.fd = -1;
        poke_worker();
}

void messagepump::update_watch(watch &w, fd_event event)
{
        lock l(cs);
        w.event = event;
        LOG("Updated watch, fd = %d, event = %d\n", w.fd, w.event);
        poke_worker();
}

messagepump::fd_event messagepump::get_events(watch &w)
{
        fd_event ret = none;
        if(FD_ISSET(w.fd, &readfds))
                ret |= read;
        if(FD_ISSET(w.fd, &writefds))
                ret |= write;
        if(FD_ISSET(w.fd, &exceptfds))
                ret |= except;
        return ret;
}

void messagepump::register_timeout(timeout &t)
{
        worker.start();
        lock l(cs);
        timeouts.push_front(t);
#ifdef DEBUG        
        timeval now;
        ::gettimeofday(&now, 0);
        LOG("Registered timeout = %li s, %li us at %li s, %li us\n", 
                t.tv.tv_sec, t.tv.tv_usec, now.tv_sec, now.tv_usec);
#endif        
        poke_worker();
}

void messagepump::unregister_timeout(timeout &t)
{
        // Create and take mutex cs.
        lock l(cs);
        LOG("Unregistered timeout at %li s, %li us\n", t.tv.tv_sec, t.tv.tv_usec);
        t.tv.tv_sec = -1;
        poke_worker();
}

void messagepump::update_timeout(timeout &t, timeval const *tv)
{
        lock l(cs);
        if (!tv) {
          tv = &null_timeout;
        }
        t.tv = *tv;
#ifdef DEBUG        
        timeval now;
        ::gettimeofday(&now, 0);
        LOG("Updated timeout = %li s, %li us at %li s, %li us\n", 
                t.tv.tv_sec, t.tv.tv_usec, now.tv_sec, now.tv_usec);
#endif        
        poke_worker();
}

void messagepump::init()
{
        struct sigaction sa;
        sa.sa_handler = &ignore;
        sa.sa_flags = 0;
        sigemptyset(&sa.sa_mask);
        sigaction(SIGUSR1, &sa, 0);

        sigset_t mask;
        sigemptyset(&mask);
        sigaddset(&mask, SIGUSR1);
        pthread_sigmask(SIG_BLOCK, &mask, 0);
}

/// @todo Exit message pump if opening resource fails.
///       Not exiting it is most likely the cause that sometimes the Release
///       configuration of the application / VISA library does not exit.
void messagepump::run()
{
        timeval now;
        ::gettimeofday(&now, 0);

        for(;;)
        {
                timeout *expired = NULL;
                bool use_timeout = false;
                timeval next;

                {
                        lock l(cs);

                        for(timeout_iterator i = timeouts.begin(); i != timeouts.end(); ++i)
                        {
                                while(i != timeouts.end() && i->tv.tv_sec == -1)
                                {
                                        timeout &t = *i;
                                        i = timeouts.erase(i);
                                        LOG("Cleaning up timeout, %li us\n", t.tv.tv_usec);
                                        t.cleanup();
                                        LOG("Cleaned up timeout\n");
                                }
                                if(i == timeouts.end())
                                        break;
                                if(i->tv == null_timeout)
                                        continue;
                                if(i->tv < now)
                                {
                                        i->tv = null_timeout;
                                        expired = &*i;
                                        break;
                                }
                                if(!use_timeout || i->tv < next)
                                {
                                        next = i->tv;
                                        use_timeout = true;
                                }
                        }
                }
//                LOG("use_timeout = %d\n", use_timeout);
                
                if(expired)
                {
                  ::gettimeofday(&now, 0);
                  LOG("Notifying about timeout at %li...\n", now.tv_sec);
                  expired->notify_timeout();
                  ::gettimeofday(&now, 0);
                  continue;
                }

                if (use_timeout)
                  next -= now;

                FD_ZERO(&readfds);
                FD_ZERO(&writefds);
                FD_ZERO(&exceptfds);

                int maxfd = -1;

                {
                        lock l(cs);

                        for (watch_iterator i = watches.begin(); i != watches.end(); ++i)
                        {
                                while (i != watches.end() && i->fd == -1)
                                {
//                                  LOG("Cleaning up watch\n");
//                                  if ((void *) *i == NULL) {
//                                    watch &w = *i;
//                                    w->cleanup();
//                                  }
//                                  else {
//                                    LOG("watch is null.\n");
//                                  }
//                                  i = watches.erase(i);
                                  
                                    watch &w = *i;
                                    i = watches.erase(i);
                                    LOG("Cleaning up watch, fd = %d\n", w.fd);
                                    w.cleanup();
                                    LOG("Cleaned up fd\n");
                                }
                                if(i == watches.end())
                                        break;

                                if(i->event & read)
                                        FD_SET(i->fd, &readfds);
                                if(i->event & write)
                                        FD_SET(i->fd, &writefds);
                                if(i->event & except)
                                        FD_SET(i->fd, &exceptfds);
                                if(i->event && i->fd > maxfd)
                                        maxfd = i->fd;
                        }

                        if(!use_timeout && maxfd == -1)
                        {
//                          LOG("Waiting for SIGUSR1...\n");
                          timespec mutex_timeout = {.tv_sec = 0, .tv_nsec = 1000000};
                          cv.wait(cs, mutex_timeout);
//                          cv.wait(cs);
                          continue;
                        }
                }

                sigset_t mask;
                sigemptyset(&mask);

                timespec next_as_ts;
                if(use_timeout)
                {
                        next_as_ts.tv_sec = next.tv_sec;
                        next_as_ts.tv_nsec = next.tv_usec * 1000;
                }
#ifdef DEBUG                
                timeval nowDebug;
                ::gettimeofday(&nowDebug, 0);
                LOG("Waiting for event at %ld s, %ld us...\n", nowDebug.tv_sec, nowDebug.tv_usec);
#endif                
                /// @todo The watch should be registered first.
                int rc = ::pselect(maxfd + 1, &readfds, &writefds, &exceptfds, use_timeout ? &next_as_ts : 0, &mask);
                if(rc == -1)
                {
                        if(errno == EINTR)
                        {
                                // Interrupted system call
                                // GFuchs: I think that error occurs when the signal gets enabled.
                                ::gettimeofday(&now, 0);
                                LOG("EINTR at %ld s, %ld us\n", now.tv_sec, now.tv_usec);
                                continue;
                        }
                        LOG("pselect returned error %d.\n", errno);
                        return;
                }

                LOG("Number of events = %d\n", rc);
                if (rc > 0)
                {
                        std::queue<watch *> notify_list;

                        {
                                lock l(cs);

                                for(watch_iterator i = watches.begin(); i != watches.end(); ++i)
                                {
                                        fd_event ev = get_events(*i);
                                        if(ev)
                                                notify_list.push(&*i);
                                }
                        }

                        while(!notify_list.empty())
                        {
                                for(watch_iterator i = watches.begin(); i != watches.end(); ++i)
                                {
                                        if(notify_list.front() == &*i)
                                        {
                                                // For vxi, notifies RPC watch (notify_rpc_watch).
                                                i->notify_fd_event(i->fd, get_events(*i));
                                                break;
                                        }
                                }
                                notify_list.pop();
                        }
                }

                ::gettimeofday(&now, 0);
        }
        LOG("Message pump thread finished.\n");
}

void messagepump::poke_worker()
{
  /// @todo Figure out how to check whether anybody is waiting for the signal.
//  volatile int lock = 0;
//  while (lock == 0) {
//    lock = cs.impl.__data.__lock;
//  }
//  while (cv.impl.__data.__nwaiters == 0) {}
  cv.signal();
  worker.kill(SIGUSR1);
}

void messagepump::ignore(int)
{
  return;
}

timeval const messagepump::null_timeout = { 0, 0 };

/**
 @todo Instantiate the message pump and start it when opening a resource manager.
 *     With other words, the resource manager should own the message pump. Then,
 *     when the resource manager is closed it can cancel the message pump thread.
 */
messagepump main;

}
