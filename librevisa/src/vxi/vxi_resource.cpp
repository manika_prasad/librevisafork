/*
 * Copyright (C) 2012 Simon Richter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "vxi_resource.h"
#include "exception.h"
#include "logger.h"
#include <vxi_intr.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <poll.h>

#include <iostream>

//#include <rpc/rpc.h>

extern "C" void device_intr_1(struct svc_req *rqstp, register SVCXPRT *transp);
//struct rpc_createerr rpc_createerr;
//*(__rpc_createerr()) error_creation= null;

namespace librevisa {
namespace vxi {

struct vxi_resource::action
{
        // completion notification
        mutex cs;
        condvar cv;

        enum {
                OPEN,
                READSTB,
                READ,
                WRITE
        } op;

        ViStatus status;

        union
        {
                struct { } open;
                struct
                {
                        unsigned char stb;
                } readstb;
                struct
                {
                        unsigned char *buffer;
                        size_t size;
                        size_t count;
                } read;
                struct
                {
                        unsigned char const *buffer;
                        size_t size;
                        size_t count;
                } write;
        };
};

struct vxi_resource::rpc_watch_proxy : messagepump::watch
{
        rpc_watch_proxy(vxi_resource &master, int fd) : master(master) { this->fd = fd; event = messagepump::read; }

        virtual void notify_fd_event(int fd, messagepump::fd_event) { master.notify_rpc_watch(fd); }
        virtual void cleanup() { }

        vxi_resource &master;
};


vxi_resource::vxi_resource(std::string const &hostname, unsigned timeout) :
        hostname(hostname),
        io_timeout(timeout), lock_timeout(5000)
{
        LOG("Instantiating resource %s.\n", hostname.c_str());
        main.register_timeout(*this);

        action ac;
        ac.op = ac.OPEN;
        perform(ac);

        /// @todo handle errors
        return;
}

vxi_resource::~vxi_resource() throw()
{
        return;
}

void vxi_resource::do_open(action &)
{
        int sock = RPC_ANYSOCK;

        addrinfo hints =
        {
                AI_ADDRCONFIG,
                AF_INET,
                SOCK_STREAM,
                IPPROTO_TCP,
                0,
                0,
                0,
                0
        };

        addrinfo *results;

        int rc = getaddrinfo(
                hostname.c_str(),
                0,
                &hints,
                &results);
        if(rc != 0)
                throw exception(VI_ERROR_RSRC_NFOUND);

        for(addrinfo *i = results; i; i = i->ai_next)
        {
              if(i->ai_addr->sa_family != AF_INET)
                      continue;

              sockaddr_in *sin = reinterpret_cast<sockaddr_in *>(i->ai_addr);
              sin->sin_port = 0;
              /// @todo Function hangs if host is not on the same network. It
              ///       does not seem to throw an exception either.
              ///       Kill after a certain timeout.
              // Lines indicated are part of clnttcp_create,
              // see https://github.com/lattera/glibc/blob/master/sunrpc/clnt_tcp.c
              // Trying to connect here to a given instrument address shows that
              // the socket's connect function is the cause of not returning.
              // --------------------------------------------------------------
//              sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
//              (void) bindresvport(sock, (struct sockaddr_in *) 0);
//              if (connect(sock, (struct sockaddr *) sin, sizeof (*sin)) < 0) {
//                  throw exception(VI_ERROR_RSRC_NFOUND);
//              }
              // --------------------------------------------------------------
              client = clnttcp_create(sin, DEVICE_CORE, DEVICE_CORE_VERSION, &sock, 0, 0);
              if (client)
                break;
        }

        if (!client)
          throw exception(VI_ERROR_RSRC_NFOUND);

        Create_LinkParms clp = { 1, false, lock_timeout, const_cast<char *>("inst0") };
        Create_LinkResp *resp = create_link_1(&clp, client);

        if (!resp)
          throw exception(VI_ERROR_RSRC_NFOUND);

        lid = resp->lid;

        SVCXPRT *intr_svc = svctcp_create(RPC_ANYSOCK, 0, 0);

        if(!intr_svc)
                throw exception(VI_ERROR_SYSTEM_ERROR);

        if(!svc_register(intr_svc, DEVICE_INTR, DEVICE_INTR_VERSION, device_intr_1, 0))
                throw exception(VI_ERROR_SYSTEM_ERROR);

        instances[hostname] = this;

        update_rpc_watches();

        sockaddr_storage ss;

        socklen_t ss_len = sizeof ss;
        rc = getsockname(sock, reinterpret_cast<sockaddr *>(&ss), &ss_len);
        if (rc == -1)
          throw exception(VI_ERROR_SYSTEM_ERROR);

        if (ss.ss_family != AF_INET)
          throw exception(VI_ERROR_SYSTEM_ERROR);

        sockaddr_in *sin = reinterpret_cast<sockaddr_in *>(&ss);

        Device_RemoteFunc rfn =
        {
                ntohl(sin->sin_addr.s_addr),
                intr_svc->xp_port,
                DEVICE_INTR,
                DEVICE_INTR_VERSION,
                DEVICE_TCP
        };

        Device_Error *error = create_intr_chan_1(&rfn, client);

        if(!error)
                throw exception(VI_ERROR_SYSTEM_ERROR);

        if(error->error == 8)
                ; /// @todo handle case where interrupt channel is unsupported
        else if(error->error)
                throw exception(VI_ERROR_SYSTEM_ERROR);

        Device_EnableSrqParms esp =
        {
                lid,
                true,
                {
                        u_int(hostname.size()),
                        const_cast<char *>(hostname.c_str())
                }
        };

        error = device_enable_srq_1(&esp, client);
        if(!error)
                throw exception(VI_ERROR_SYSTEM_ERROR);
        if(error->error)
                throw exception(VI_ERROR_SYSTEM_ERROR);

        /// @todo handle errors
        return;
}

ViStatus vxi_resource::Close()
{
//        action *ac = actions.front();
//        if (ac != NULL)
//          lock l(ac->cs);
        LOG("Closing resource...\n");
        destroy_link_1(&lid, client);
        clnt_destroy(client);
        instances.erase(hostname);

        main.unregister_timeout(*this);
        /// @todo At this point the message pump is waiting for an FD event.
        ///       Kick it out of it so it can run its loop acting upon the
        ///       un-registrations.
//        notify_rpc_watch();
//        FD_SET(fd, &readfds);
        while (tv.tv_sec != -1);
        std::list<rpc_watch_proxy>::iterator i = rpc_watch_proxies.begin();
        for (; i != rpc_watch_proxies.end(); i++) {
          main.unregister_watch(*i);
          // This does not kick the message pump.
//          main.update_watch(*i, messagepump::read);
        }
        rpc_watch_proxies.clear();

        delete this;
        LOG("Closed resource.\n");
        return VI_SUCCESS;
}

ViStatus vxi_resource::Lock(ViAccessMode, ViUInt32, ViKeyId, ViKeyId)
{
        return VI_SUCCESS;
}

ViStatus vxi_resource::Unlock()
{
        return VI_SUCCESS;
}

ViStatus vxi_resource::GetAttribute(ViAttr attr, void *attrState)
{
//        return resource::GetAttribute(attr, attrState);
  switch (attr)
  {
  case VI_ATTR_TMO_VALUE:
    *reinterpret_cast<ViVersion *>(attrState) = (unsigned) io_timeout;
    return VI_SUCCESS;

  default:
    return VI_ERROR_NSUP_ATTR;
  }
}

ViStatus vxi_resource::SetAttribute(ViAttr attr, ViAttrState attrState)
{
//        return resource::SetAttribute(attr, attrState);
  switch (attr)
  {
  case VI_ATTR_TMO_VALUE:
    io_timeout = (u_long) reinterpret_cast<ViVersion *>(attrState);
    return VI_SUCCESS;

  default:
    return VI_ERROR_NSUP_ATTR;
  }
}

ViStatus vxi_resource::Read(ViBuf buf, ViUInt32 count, ViUInt32 *retCount)
{
        action ac;
        ac.op = ac.READ;
        ac.read.buffer = buf;
        ac.read.size = count;
        perform(ac);
        *retCount = ac.read.count;
        return ac.status;
}

void vxi_resource::do_read(action &ac)
{
        Device_ReadParms read_parms =
        {
                lid,
                ac.read.size,
                io_timeout,
                lock_timeout,
                0,
                0
        };

        Device_ReadResp *read_resp = device_read_1(&read_parms, client);

        ::memcpy(ac.read.buffer, read_resp->data.data_val, read_resp->data.data_len);
        ac.read.count = read_resp->data.data_len;
        /// @todo handle errors
        ac.status = VI_SUCCESS;
}

ViStatus vxi_resource::Write(ViBuf buf, ViUInt32 count, ViUInt32 *retCount)
{
        action ac;
        ac.op = ac.WRITE;
        ac.write.buffer = buf;
        ac.write.size = count;
        perform(ac);
        *retCount = ac.write.count;
        return ac.status;
}

void vxi_resource::do_write(action &ac)
{
        Device_WriteParms write_parms =
        {
                lid,
                io_timeout,
                lock_timeout,
                8,
                {
                        static_cast<u_int>(ac.write.size),
                        reinterpret_cast<char *>(const_cast<unsigned char *>(ac.write.buffer))
                }
        };

        Device_WriteResp *write_resp = device_write_1(&write_parms, client);

        if(!write_resp)
        {
                ac.status = VI_ERROR_SYSTEM_ERROR;
                return;
        }

        ac.write.count = write_resp->size;

        /// @todo handle errors
        ac.status = VI_SUCCESS;
}

ViStatus vxi_resource::ReadSTB(ViUInt16 *retStatus)
{
        action ac;
        ac.op = ac.READSTB;
        perform(ac);
        *retStatus = ac.readstb.stb;
        return ac.status;
}

void vxi_resource::do_readstb(action &ac)
{
        Device_GenericParms dgp =
        {
                lid,
                0,
                lock_timeout,
                io_timeout
        };
        Device_ReadStbResp *resp = device_readstb_1(&dgp, client);

        /// @todo handle errors

        ac.readstb.stb = resp->stb;
        ac.status = VI_SUCCESS;
}

void vxi_resource::update_rpc_watches()
{
        std::list<rpc_watch_proxy>::iterator i = rpc_watch_proxies.begin();
        for(int fd = 0; fd < FD_SETSIZE; ++fd)
        {
                while(i != rpc_watch_proxies.end() && i->fd < fd)
                        ++i;

                bool const is_current = (i != rpc_watch_proxies.end() && i->fd == fd);
                bool const active = FD_ISSET(fd, &svc_fdset);

                if(active)
                {
                        if(!is_current)
                        {
                                // need to insert
                                i = rpc_watch_proxies.insert(i, rpc_watch_proxy(*this, fd));
                                main.register_watch(*i);
                        }
                        else
                        {
                                main.update_watch(*i, messagepump::read);
                        }
                }
                else if(is_current)
                {
                        main.update_watch(*i, messagepump::none);
                }
        }
}

void vxi_resource::notify_rpc_watch(int fd)
{
        fd_set readfds;
        FD_ZERO(&readfds);
        FD_SET(fd, &readfds);
        svc_getreqset(&readfds);
        update_rpc_watches();
}

void vxi_resource::notify_timeout()
{
        if(actions.empty())
                return;

        action &ac = *actions.front();

        {
                lock l(ac.cs);

          try {
                switch(ac.op)
                {
                  // We timed out when trying to open.
                case action::OPEN:
   //               do_open(ac);
                  timedOut = true;
                  break;
                case action::READSTB:           do_readstb(ac); break;
                case action::READ:              do_read(ac);    break;
                case action::WRITE:             do_write(ac);   break;
                }
              }
              catch(exception const& e) {
                LOG("Visa exception. Status = 0x%08X.\n", (unsigned) e.code);
              }
              catch(...) {
                LOG("vxi_resource::notify_timeout caught exception\n");
              }
              ac.cv.signal();
        }
        actions.pop();
}

void vxi_resource::cleanup()
{
        return;
}

void vxi_resource::perform(action &ac)
{
        lock l(ac.cs);
        actions.push(&ac);

        timeval now;
        ::gettimeofday(&now, 0);
        now.tv_sec += io_timeout / 1000;
        main.update_timeout(*this, &now);

        ac.cv.wait(ac.cs);
}

void vxi_resource::emit(std::string const &cookie, ViEventType event)
{
        std::map<std::string, vxi_resource *>::iterator i = instances.find(cookie);
        if(i == instances.end()) {
                printf("Aborting...\n");
                abort();
        }
        // vxi_resource::emit
        i->second->emit(event);
}

std::map<std::string, vxi_resource *> vxi_resource::instances;

}
}

void *device_intr_srq_1_svc(Device_SrqParms *srqp, svc_req *)
{
        printf("Device service request\n");
        std::string cookie(srqp->handle.handle_val, srqp->handle.handle_len);
        librevisa::vxi::vxi_resource::emit(cookie, VI_EVENT_SERVICE_REQ);
        static unsigned int const zero = 0;
        return const_cast<unsigned int *>(&zero);
}

