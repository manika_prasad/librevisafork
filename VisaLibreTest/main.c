/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: gfuchs
 *
 * Created on July 3, 2016, 10:48 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include "visa.h"

/**
 * This is the entry function of the application.
 * It opens a default resource manager session and prints the session value.
 * @return status of the operation
 */
int main() {
  ViSession rmSession;
  ViStatus status = viOpenDefaultRM(&rmSession);
  if (status != VI_SUCCESS) {
    (void) printf("Error opening session for default resource manager. status = 0x%08X.\n", 
            (unsigned) status);
    return EXIT_FAILURE;
  }
  (void) printf("Resource Manager session = 0x%08X.\n", (unsigned) rmSession);
  
  unsigned long attributeValue;
  status = viGetAttribute(rmSession, VI_ATTR_RSRC_MANF_ID, (void *) &attributeValue);
  if (status != VI_SUCCESS) {
    (void) printf("Error getting attribute for manufacturer id. status = 0x%08X.\n",
            (unsigned) status);
  }
  else {
    (void) printf("Manufacturer id = 0x%08X.\n", (unsigned) attributeValue);
  }
  status = viGetAttribute(rmSession, VI_ATTR_RSRC_IMPL_VERSION, (void *) &attributeValue);
  if (status != VI_SUCCESS) {
    (void) printf("Error getting attribute for implementation version. status = 0x%08X.\n",
            (unsigned) status);
  }
  else {
    (void) printf("Implementation version = %u.\n", (unsigned) attributeValue);
  }
  
  char attributeString[32];
  status = viGetAttribute(rmSession, VI_ATTR_RSRC_MANF_NAME, attributeString);
  if (status != VI_SUCCESS) {
    (void) printf("Error getting attribute for manufacturer name. status = 0x%08X.\n",
            (unsigned) status);
  }
  else {
    (void) printf("Manufacturer name = %s.\n", attributeString);
  }

  ViSession instrumentSession;
//  ViRsrc instrumentName = "TCPIP::138.67.34.170::INSTR";
  ViRsrc instrumentName = "TCPIP::192.168.1.104::INSTR";
  /// @todo Loop succeeds only the first two times. Trying to open the instrument
  ///       session a third time hangs the application.
//  for (int i = 0; i < 1; i++) {
  for (;;) {
    /// @todo Timeout does not work.
    status = viOpen(rmSession, instrumentName, 0, 2000, &instrumentSession);
    if (status != VI_SUCCESS) {
      (void) printf("ViStatus = 0x%08X.\n", (unsigned) status);
      return EXIT_FAILURE;
    }
    else {
      (void) printf("Instrument handle = 0x%08X.\n", (unsigned) instrumentSession);
      status = viClose(instrumentSession);
      (void) printf("Instrument handle 0x%08X closed.\n", (unsigned) instrumentSession);
    }
    printf("Press c (continue) or x (exit) and the return key...");
    int cIn = getchar();
    getchar();
    if (cIn == 'x')
      break;
  }
  if (status == VI_SUCCESS) {
    status = viClose(rmSession);
    (void) printf("Manager handle 0x%08X closed. ViStatus = 0x%08X.\r\n", (unsigned) rmSession, (unsigned) status);
    return status;
  }
  return status;
  
  // Try to open non-existing instrument.
//  instrumentName = "TCPIP::192.168.1.99::INSTR";
//  status = viOpen(rmSession, instrumentName, 0, 2000, &instrumentSession);
//  if (status == VI_SUCCESS) {
//    (void) printf("Instrument handle = 0x%08X.\r\n", (unsigned) instrumentSession);
//    viClose(rmSession);
//    return EXIT_FAILURE;
//  }
//  else {
//    (void) printf("ViStatus = 0x%08X.\r\n", (unsigned) status);
//  }
  return EXIT_SUCCESS;
}
