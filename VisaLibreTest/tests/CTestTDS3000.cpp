/**
 * @license

Copyright 2018 Günter (gfuchs@acousticmicroscopy.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* 
* @version 0.1
 */

/*
 * File:   CTestTDS3000.cpp
 * Author: gfuchs (gfuchs@acousticmicroscopy.com)
 *
 * Created on Mar 4, 2018, 4:58:03 PM
 */

#include "CTestTDS3000.h"


CPPUNIT_TEST_SUITE_REGISTRATION(CTestTDS3000);

ViSession CTestTDS3000::rmSession = 0;
ViSession CTestTDS3000::instrumentSession = 0;

CTestTDS3000::CTestTDS3000() {
}

CTestTDS3000::~CTestTDS3000() {
  viClose(rmSession);
}

void CTestTDS3000::setUp() {
  if (rmSession == 0) {
    instrumentSession = 0;
    ViStatus status = viOpenDefaultRM(&rmSession);
    if (status != VI_SUCCESS) {
      (void) printf("Error opening session for default resource manager. status = 0x%08X.\r\n", 
              (unsigned) status);
      return;
    }
    (void) printf("Resource Manager session = 0x%08X.\r\n", (unsigned) rmSession);
  }
}

void CTestTDS3000::tearDown() {
}

void CTestTDS3000::testMethod_viOpen() {
  try {
    ViRsrc instrumentName = (ViRsrc) "TCPIP::192.168.1.106::INSTR";
    status = viOpen(rmSession, instrumentName, 0, 2000, &instrumentSession);
    snprintf(messageBuffer, sizeof messageBuffer, "ViStatus = 0x%08X.", (unsigned) status);
    CPPUNIT_ASSERT_MESSAGE(messageBuffer, status == VI_SUCCESS);
    (void) printf("Instrument session = 0x%08X.\r\n", (unsigned) instrumentSession);
  }
  catch (librevisa::exception) {
    CPPUNIT_ASSERT_MESSAGE("libreVisa threw an exception.", false);
  }
}

void CTestTDS3000::testMethod_viClose() {
  if (instrumentSession == 0) {
    CPPUNIT_ASSERT(instrumentSession == 0);
    return;
  }
  status = viClose(instrumentSession);
  snprintf(messageBuffer, sizeof messageBuffer, "ViStatus = 0x%08X.\r\n", (unsigned) status);
  CPPUNIT_ASSERT_MESSAGE(messageBuffer, status == VI_SUCCESS);
  instrumentSession = 0;
}

void CTestTDS3000::testFailedMethod_viOpen() {
  try {
    ViRsrc instrumentName = (ViRsrc) "TCPIP::192.168.1.99::INSTR";
    status = viOpen(rmSession, instrumentName, 0, 2000, &instrumentSession);
    snprintf(messageBuffer, sizeof messageBuffer, "ViStatus = 0x%08X.", (unsigned) status);
    CPPUNIT_ASSERT_MESSAGE(messageBuffer, status != VI_SUCCESS);
    (void) printf("%s\r\n", messageBuffer);
  }
  catch (librevisa::exception) {
    CPPUNIT_ASSERT_MESSAGE("libreVisa threw an exception.", false);
  }
}


