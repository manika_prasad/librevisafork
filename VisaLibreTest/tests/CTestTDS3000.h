/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   CTestTDS3000.h
 * Author: gfuchs
 *
 * Created on Mar 4, 2018, 4:58:03 PM
 */

#ifndef CTESTTDS3000_H
#define CTESTTDS3000_H

#include <cppunit/extensions/HelperMacros.h>
#include "visa.h"
#include "exception.h"


class CTestTDS3000 : public CPPUNIT_NS::TestFixture {
  CPPUNIT_TEST_SUITE(CTestTDS3000);

  /// @todo Calling testFailedMethod_viOpen after testMethod_viClose causes a
  ///       segmentation fault.
  CPPUNIT_TEST(testMethod_viOpen);
  CPPUNIT_TEST(testMethod_viClose);
  CPPUNIT_TEST(testFailedMethod_viOpen);

  CPPUNIT_TEST_SUITE_END();

public:
  CTestTDS3000();
  virtual ~CTestTDS3000();
  void setUp();
  void tearDown();

private:
  void testMethod_viOpen();
  void testMethod_viClose();
  void testFailedMethod_viOpen();
  ViStatus status;
  static ViSession rmSession;
  static ViSession instrumentSession;
  char messageBuffer[1024];
};

#endif /* CTESTTDS3000_H */

